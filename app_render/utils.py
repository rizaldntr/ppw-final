from .models import Pengguna

def get_data_user(request, tipe):
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']

    return data

def create_new_user(request):
    nama = get_data_user(request, 'user_login')
    kode_identitas = get_data_user(request, 'kode_identitas')

    pengguna = Pengguna()
    pengguna.kode_identitas = kode_identitas
    pengguna.nama = nama
    pengguna.save()

    return pengguna

def get_parameter_request(request):
    if request.GET.get("judul"):
        judul = request.GET.get("judul")
    else:
        judul = "-"

    if request.GET.get("tahun"):
        tahun = request.GET.get("tahun")
    else:
        tahun = "-"

    return judul, tahun
